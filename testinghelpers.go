package testinghelpers

import (
	"fmt"
	"os"
	"os/exec"
	"reflect"
	"testing"
	"time"
)

//TestingTableStrings is a testing table containing strings where the Input and Expected values are equal.
var TestingTableStrings = []struct {
	Input    string
	Expected string
}{
	{"", ""},
	{"a", "a"},
	{"1", "1"},
	{"Test", "Test"},
	{"2Test", "2Test"},
	{"Test with spaces", "Test with spaces"},
	{"ячмтьбю.", "ячмтьбю."},
}

/*Implements tests if a structure implements an interface. If the interfaces is not implimented, an error is thrown.
  Both structure and inter must be pointers.
*/
func Implements(structure interface{}, inter interface{}, t *testing.T) {
	interType := reflect.TypeOf(inter).Elem()
	implements := reflect.TypeOf(structure).Implements(interType)

	if !implements {
		t.Errorf("%s does not implement %s.", reflect.TypeOf(structure), interType)
	}
}

/*ValueEquals converts two structs to string values and compares them.
  It is useful if reflect.DeepEqual fails to recognize two structs as equal.
  Returns true if the two structs string representation is equal.
*/
func ValueEquals(foo interface{}, bar interface{}) bool {
	return fmt.Sprintf("%v", foo) == fmt.Sprintf("%v", bar)
}

//TimeWithinThreshold checks to see if the difference between time t1 and t2 is within the specified threshold.
func TimeWithinThreshold(t1 time.Time, t2 time.Time, threshold time.Duration) bool {
	return t1.Sub(t2) >= threshold
}

//TimeWithinThreshold checks to see if the difference between time t and now is within the specified threshold.
func TimeWithinThresholdFromNow(t1 time.Time, threshold time.Duration) bool {
	return t1.Sub(time.Now()) >= threshold
}

/*ExitCodeTest is used in the testing of a testing function that exits the program and returns an error code.
  function - is the testing function being tested.
  funcName - is the name of the function calling ExitCodeTest.
  t - the testing.T struct to be passed into 'function'.
  exitCode - the exit code that the function is expected to return.
*/
func ExitCodeTest(function func(*testing.T), funcName string, t *testing.T, exitCode int) {

	if os.Getenv("TEST_CRASH") == "1" {
		function(t)
		return
	}

	cmdParameter := fmt.Sprintf("-test.run=%s", funcName)
	cmd := exec.Command(os.Args[0], cmdParameter)
	cmd.Env = append(os.Environ(), "TEST_CRASH=1")
	err := cmd.Run()
	e, _ := err.(*exec.ExitError) // Do we want the second variable?
	if e.ExitCode() != exitCode {
		t.Fatalf("%s() wanted exit code %d, but received %d", reflect.TypeOf(function), exitCode, e.ExitCode())
	}

}
